# Angwandte Programmierung & PyTorch
(FOM Masterkurs "Big Data Analytics", Dozent P. Koch, M. Sc.)
___

# Willkommen im Bitbucket Repository zur Vorlesung "Angewandte Prgrammierung & PyTorch"

*Bitte beachten Sie, dass das Repository kostanten Erweiterungen unterworfen ist, weitere Inhalte werden im Laufe des Semesters bereitgestellt.*

**Inhalte Vorlesung 01:**

- N1 - Datentypen und Strukturen in Python
- N2 - Funktionen in Python
- N3 - Objektorientierung in Python
- N4 - Numpy Library
- N5 - Pandas Library
- N6 - Scikit-learn Library

**Weitere geplante Themen im Rahmen der Vorlesung:**

- Einführung PyTorch
- Advanced Deep Learning in PyTorch
	- Convolutional Neural Networks
	- Autoencoder
	- Denoising Autoencoder
	- Variational Autoencoder
	- Generative Adversarial Networks
		- Wasserstein GANs
		- Wasserstein Gradient Penalty GANs
	- Recurrent Neural Networks
	

