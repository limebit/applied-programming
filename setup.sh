python3 -m venv .env
source .env/bin/activate
pip install ipykernel
pip install -r requirements.txt
ipython kernel install --user --name=applied-programming